
const express = require('express');
const Mongoose = require('mongoose');
const db = require('./config/keys').mongoUrl;
// 引入users.js
const users = require("./routes/api/users");
const profiles = require("./routes/api/profiles");

//body-parser
const bodyParser = require('body-parser');
const passport = require('passport');

const app = express();
app.get("/",(req,res)=>{
    res.send("Hello");
})

Mongoose.connect(db)
        .then(()=>console.log("mongodb connected"))
        .catch(err=>console.log(err));

// MongeoClient.connect(Config.dbUrl,(err,client)=>{
//     if(err){
//         console.log(err);
//         console.log("err");
//         //reject(err);
//     }else{
//        console.log("success");
//     }
// })

//passport初始化
app.use(passport.initialize());
require("./config/passport")(passport);

//使用body-parser
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

// 使用routers
app.use("/api/users",users);
app.use("/api/profiles",profiles);

const port = process.env.PORT||5000;
app.listen(port,()=>{
    console.log(`Server running on port ${port}`);
})