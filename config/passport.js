const passport = require("passport");

const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
    const mongoose = require("mongoose");
    const User = mongoose.model("user");
    const keys = require("../config/keys");
const { findOne } = require("../models/User");
const { use } = require("passport");
    var opts = {};// 配置
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = keys.secretOrKey;

module.exports = passport => {
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        User.findById(jwt_payload.id)
        .then(user => {
            if(user){
                return done(null,user);
            }else{
                return done(null,false);
            }
            
        })
        .catch(err =>console.log("passport"+err));
    }))
}