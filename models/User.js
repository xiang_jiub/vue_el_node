
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;



const UserSchema = new Schema({
    name:{
        type:String,
        require:true
    },
    email:{
        type:String,
        require:true
    },
    password:{
        type:String,
        require:true
    },
    avatar:{
        type:String
    },
    identity:{
        type:String,
        require:true
    },
    date:{
        type:Date,
        require:Date.now
    }
})


module.exports = User = Mongoose.model("user",UserSchema);