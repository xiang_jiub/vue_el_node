
const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;



const ProfileSchema = new Schema({
    // 类型
    type:{
        type:String,
    },
    // 描述
    describe:{
        type:String,
    },
    // 收入
    incode:{
        type:String,
        require:true
    },
    // 支出
    expend:{
        type:String,
        require:true
    },
    //现金
    cash:{
        type:String,
        require:true
    },
    // 备注
    remark:{
        type:String
    },
    date:{
        type:Date,
        require:Date.now
    }
})


module.exports = Profile = Mongoose.model("profile",ProfileSchema);