// 登陆和注册
const express = require("express");
const router = express.Router();
const passport = require("passport");
const Profile = require('../../models/mProfile');
const { json } = require("body-parser");
// $route GET api/profiles/test
// @desc 返回的请求的json数据
// @access public
router.get("/test",(req,res)=>{
    res.json({msg:"profiles works"});
})

// $route POST api/profiles/add
// @desc 创建接口信息,添加信息
// @access Private
 router.post('/add',passport.authenticate('jwt',{session:false}),(req,res)=>{
    //内容事存在的
    const porfileFields = {};
    if(req.body.type) porfileFields.type = req.body.type;
    if(req.body.describe) porfileFields.describe = req.body.describe;
    if(req.body.incode) porfileFields.incode = req.body.incode;
    if(req.body.expend) porfileFields.expend = req.body.expend;
    if(req.body.cash) porfileFields.cash = req.body.cash;
    if(req.body.remark) porfileFields.remark = req.body.remark;

    new  Profile(porfileFields).save().then(result => {
        res.json(result);
    });
})
// $route GET api/profiles/getDataList
// @desc 获取所以信息
// @access Private
router.get('/getAllDataList',passport.authenticate('jwt',{session:false}),(req,res)=>{
    Profile.find()
    .then(result =>{
        if(!result){
            return res.json("没有任何内容");
        }
        res.json(result);
    })
    .catch(err => res.status(404).json(err));
});

// $route GET api/profiles/getDataList:id
// @desc 获取某个信息
// @access Private
router.get('/:id',passport.authenticate('jwt',{session:false}),(req,res)=>{
    Profile.find({_id:req.params.id})
    .then(result =>{
        if(!result){
            return res.json("没有任何内容");
        }
        res.json(result);
    })
    .catch(err => res.status(404).json(err));
});

module.exports = router;